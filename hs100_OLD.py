import urllib2
import json
import logging
import time
import os

froniusip = '192.168.82.81'
hs100ip = '192.168.82.82'
hs100com = 'perl /usr/local/scripts/hs100.pl -ip ' + str(hs100ip) + ' -command '
froniusurl = 'http://' + str(froniusip) + '/solar_api/v1/GetMeterRealtimeData.cgi?Scope=Device&DeviceId=0'
hs100power = 350
powercounton = 0
powercountoff = 0
efeed = 0
statuscode = 1001
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',filename='hs100.log',level=logging.DEBUG)


#Initial Power off
os.system(hs100com + str('off'))
hs100status = 0
logging.debug("Initial Power OFF")

#Wait 3 Sec
def wait():
  time.sleep(3)

#ON if Feed Power is over 10 checks over HS100 Power
def hson():
  global powercounton, powercountoff, hs100status
  powercounton = powercounton + 1
  powercountoff = 0
  print('efeed: ' + str(efeed) + 'W | HS100Power: ' + str(hs100power) + 'W')
  if powercounton > 9 and hs100status == 0:
    os.system(hs100com + str('on'))
    logging.debug('HS100 with IP ' + str(hs100ip) + ' switch to ON')
    hs100status = 1
    print('HS100: ON')
    wait()
  else:
    print('PowercountON is ' + str(powercounton))
    wait()

#OFF if Feed Power os over 5 checks lower than HS100 Power
def hsoff():
  global powercounton, powercountoff, hs100status
  powercountoff += 1
  powercounton = 0
  print('efeed: ' + str(efeed) + 'W | HS100Power: ' + str(hs100power) + 'W')
  if powercountoff > 5 and hs100status == 1:
    os.system(hs100com + str('off'))
    logging.debug('HS100 with IP ' + str(hs100ip) + ' switch to OFF')
    hs100status = 0
    print('HS100: OFF')
    wait()
  else:
    print('PowercountOFF is ' +str(powercountoff))
    wait()

#Loop and check how much power is available
while True:
  try:
    data = json.load(urllib2.urlopen(froniusurl))
    statuscode = data["Head"]["Status"]["Code"]
    if statuscode == 0:
      efeed = int(data["Body"]["Data"]["PowerReal_P_Sum"])*-1
      if hs100status == 1:  #Check if Power is on
        hs100power2 = 0 #Set Power of hs100 to 0 if running for calc power
      else:
        hs100power2 = hs100power
      if efeed > hs100power2: # Function hson if enough Power
        hson()
      else:
        hsoff()
    else:
      efeed = 0
      logging.debug('Status not OK (' + statuscode + ')')
      print('Status not OK (' + statuscode + ')')
      hsoff()
      time.sleep(30)
  except:
    logging.debug("exception erreicht")
    print('Exception')
    hsoff()
    time.sleep(60)
